

# 1.
# Вх: список чисел, Возвр: список чисел, где
# повторяющиеся числа урезаны до одного
# пример [0, 2, 2, 3] returns [0, 2, 3].
def distinct(nums):
    result = []
    values = set()

    for num in nums:
        if num not in values:
            values.add(num)
            result.append(num)

    return result


# 2. Вх: Два списка упорядоченных по возрастанию, Возвр: новый отсортированный объединенный список
def merge(list1, list2):
    index1 = 0
    index2 = 0
    result = []

    while index1 < len(list1) and index2 < len(list2):
        if list1[index1] <= list2[index2]:
            result.append(list1[index1])
            index1 += 1
        else:
            result.append(list2[index2])
            index2 += 1

    remaining = list1[index1:] + list2[index2:]
    result.extend(remaining)

    return result
