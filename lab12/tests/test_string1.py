import unittest
from lab12 import string1


class NumOfItemsTests(unittest.TestCase):
	def test_many(self):
		expected = "Number of: many"
		self.assertEqual(expected, string1.num_of_items(10))
		self.assertEqual(expected, string1.num_of_items(10.1))

	def test_count(self):
		count = -1
		expected = "Number of: " + str(count)
		self.assertEqual(expected, string1.num_of_items(count))


class StartEndSymbolsTests(unittest.TestCase):
	def test_equal_start_end(self):
		expected = "1111"
		input_string = "1150511"
		self.assertEqual(expected, string1.start_end_symbols(input_string))

	def test_start_end_only(self):
		expected = "qwer"
		input_string = "qwer"
		self.assertEqual(expected, string1.start_end_symbols(input_string))

	def test_empty_input(self):
		expected = ""
		input_string = ""
		self.assertEqual(expected, string1.start_end_symbols(input_string))
		self.assertEqual(expected, string1.start_end_symbols(None))

	def test_short_input(self):
		expected = "66"
		input_string = "6"
		self.assertEqual(expected, string1.start_end_symbols(input_string))


class ReplaceCharTests(unittest.TestCase):
	def test_example(self):
		expected = 'bi**le'
		input_string = 'bibble'
		self.assertEqual(expected, string1.replace_char(input_string))

	def test_one_char_only(self):
		expected = input_string = 'b'
		input_string = 'b'
		self.assertEqual(expected, string1.replace_char(input_string))

	def test_no_replacement(self):
		expected = input_string = 'baw'
		input_string = 'baw'
		self.assertEqual(expected, string1.replace_char(input_string))


class StrMixTests(unittest.TestCase):
	def test_examples(self):
		self.assertEqual('pix mad', string1.str_mix('max', 'pid'))
		self.assertEqual('dig donner', string1.str_mix('dog', 'dinner'))

	def test_twoChar_words(self):
		self.assertEqual('pi ma', string1.str_mix('ma', 'pi'))

	def test_oneChar_words(self):
		self.assertEqual('p m', string1.str_mix('m', 'p'))

	def test_one_word_only(self):
		self.assertEqual('p ', string1.str_mix('', 'p'))
		self.assertEqual('p  ', string1.str_mix(' ', 'p'))
		self.assertEqual('  p', string1.str_mix('p', ' '))


if __name__ == '__main__':
	unittest.main()
