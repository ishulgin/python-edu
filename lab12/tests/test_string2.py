import unittest
from lab12 import string2


class VTests(unittest.TestCase):
    def test_less_than4(self):
        expected = "asd"
        self.assertEqual(expected, string2.v(expected))

    def test_ing(self):
        argument = "qweq"
        expected = argument + "ing"
        self.assertEqual(expected, string2.v(argument))

    def test_ly(self):
        argument = "qweqing"
        expected = argument + "ly"
        self.assertEqual(expected, string2.v(argument))


class NBTests(unittest.TestCase):
    def test_simple_replace(self):
        argument = "not bad"
        expected = "good"
        self.assertEqual(expected, string2.nb(argument))

    def test_replace_before_punctuation_mark(self):
        argument = "not bad!"
        expected = "good!"
        self.assertEqual(expected, string2.nb(argument))

    def test_replace_with_word_in_between(self):
        argument = "not so bad"
        expected = "good"
        self.assertEqual(expected, string2.nb(argument))

    def test_no_replace_upper_case(self):
        expected = "Not so bad"
        self.assertEqual(expected, string2.nb(expected))

    def test_no_replace_word_before(self):
        expected = "anot so bad"
        self.assertEqual(expected, string2.nb(expected))

    def test_no_replace_word_after(self):
        expected = "not so bada"
        self.assertEqual(expected, string2.nb(expected))

    def test_no_replace_no_break(self):
        expected = "notbad"
        self.assertEqual(expected, string2.nb(expected))


if __name__ == '__main__':
    unittest.main()
