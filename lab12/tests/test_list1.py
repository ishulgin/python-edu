import unittest
from lab12 import list1


class MeTests(unittest.TestCase):
	def test_empty_result(self):
		expected = 0
		input_strings = ['qq', 'wwq']
		self.assertEqual(expected, list1.me(input_strings))

	def test_result(self):
		expected = 1
		input_words = ['qqq', ]
		self.assertEqual(expected, list1.me(input_words))

	def test_empty_input(self):
		expected = 0
		self.assertEqual(expected, list1.me([]))
		self.assertEqual(expected, list1.me({}))
		self.assertEqual(expected, list1.me(None))

	def test_different_languages(self):
		expected = 0
		input_words = ['aquа', ]
		self.assertEqual(expected, list1.me(input_words))


class FxTests(unittest.TestCase):
	def test_example(self):
		expected = ['xacadu', 'xyz', 'aabbbccc', 'apple', 'tix']
		input_strings = ['tix', 'xyz', 'apple', 'xacadu', 'aabbbccc']
		self.assertEqual(expected, list1.fx(input_strings))

	def test_x_only(self):
		expected = ['xacadu', 'xyz']
		input_strings = ['xyz', 'xacadu']
		self.assertEqual(expected, list1.fx(input_strings))

	def test_full_alphabet(self):
		expected = ['xacadu', 'xyz', 'aabbbccc', 'z']
		input_strings = ['z', 'xyz', 'aabbbccc', 'xacadu']
		self.assertEqual(expected, list1.fx(input_strings))

	def test_no_x(self):
		expected = ['apple', 'tix']
		input_strings = ['tix', 'apple']
		self.assertEqual(expected, list1.fx(input_strings))

	def test_empty_input(self):
		expected = []
		self.assertEqual(expected, list1.fx([]))
		self.assertEqual(expected, list1.fx(None))


class FuncTests(unittest.TestCase):
	def test_example(self):
		expected = [(2, 2), (1, 3), (3, 4, 5), (1, 7)]
		input_strings = [(1, 7), (1, 3), (3, 4, 5), (2, 2)]
		self.assertEqual(expected, list1.fx(input_strings))

	# TODO don't take into account other numbers (don't sort) if target numbers are equal
	# def test_equal_tuples(self):
	# 	expected = [(2, 2), (1, 2)]
	# 	input_strings = [(2, 2), (1, 2)]
	# 	self.assertEqual(expected, list1.fx(input_strings))


if __name__ == '__main__':
	unittest.main()
