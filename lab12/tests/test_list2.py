import unittest
from lab12 import list2


class DistinctTests(unittest.TestCase):
    def test_empty(self):
        expected = []
        self.assertEqual(expected, list2.distinct([]))

    def test_ordered(self):
        argument = [100, 200, 200, 200, 300]
        expected = [100, 200, 300]
        self.assertEqual(expected, list2.distinct(argument))

    def test_not_ordered(self):
        argument = [200, 100, 200, 30, 200, 20]
        expected = [200, 100, 30, 20]
        self.assertEqual(expected, list2.distinct(argument))


class MergeTests(unittest.TestCase):
    def test_empty(self):
        expected = []
        self.assertEqual(expected, list2.merge([], []))

    def test_first_empty(self):
        expected = [1, 2, 3]
        self.assertEqual(expected, list2.merge([], expected))

    def test_second_empty(self):
        expected = [1, 2, 3]
        self.assertEqual(expected, list2.merge(expected, []))

    def test_simple1(self):
        first = [1, 3, 5]
        second = [2, 4, 6]
        expected = [1, 2, 3, 4, 5, 6]
        self.assertEqual(expected, list2.merge(first, second))

    def test_simple2(self):
        first = [1, 2, 3]
        second = [4, 5, 6]
        expected = [1, 2, 3, 4, 5, 6]
        self.assertEqual(expected, list2.merge(first, second))

    def test_first_longer(self):
        first = [1, 3, 5]
        second = [2]
        expected = [1, 2, 3, 5]
        self.assertEqual(expected, list2.merge(first, second))

    def test_second_longer(self):
        first = [1]
        second = [2, 4, 6]
        expected = [1, 2, 4, 6]
        self.assertEqual(expected, list2.merge(first, second))

    def test_equal_elements(self):
        first = [1, 2]
        second = [1, 2]
        expected = [1, 1, 2, 2]
        self.assertEqual(expected, list2.merge(first, second))


if __name__ == '__main__':
    unittest.main()
