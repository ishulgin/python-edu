

# 1.
# Вх: список строк, Возвр: кол-во строк
# где строка > 2 символов и первый символ == последнему
def me(words):
	count = 0
	if words is not None and len(words) > 0:
		for word in words:
			if len(word) > 2 and word[0] == word[-1]:
				count += 1
	return count


# 2.
# Вх: список строк, Возвр: список со строками (упорядочено)
# за искл всех строк начинающихся с 'x', которые попадают в начало списка.
# ['tix', 'xyz', 'apple', 'xacadu', 'aabbbccc'] -> ['xacadu', 'xyz', 'aabbbccc', 'apple', 'tix']
def fx(words):
	if words is None or len(words) == 0:
		return []

	sorted_words = sorted(words)
	x_words, other_words = [], []
	for word in sorted_words:
		if word[0] == 'x':
			x_words.append(word)
		else:
			other_words.append(word)
	return x_words + other_words


# 3.
# Вх: список непустых кортежей,
# Возвр: список сортир по возрастанию последнего элемента в каждом корт.
# [(1, 7), (1, 3), (3, 4, 5), (2, 2)] -> [(2, 2), (1, 3), (3, 4, 5), (1, 7)]
def func(tuples):
	return sorted(tuples, key=lambda item: item[-1])
