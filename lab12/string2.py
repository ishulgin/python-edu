import re


# 1.
# Вх: строка. Если длина > 3, добавить в конец "ing",
# если в конце нет уже "ing", иначе добавить "ly".
def v(s):
    return (s + "ly" if s.endswith("ing") else s + "ing") if len(s) > 3 else s


# 1.
# Вх: строка. Если длина > 3, добавить в конец "ing",
# если в конце нет уже "ing", иначе добавить "ly".
def nb(s):
    return re.sub(r"\bnot\b.*\bbad\b", "good", s)
