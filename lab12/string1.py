

# 1.
# Входящие параметры: int <count> ,
# Результат: string в форме
# "Number of: <count>", где <count> число из вход.парам.
#  Если число равно 10 или более, напечатать "many"
#  вместо <count>
#  Пример: (5) -> "Number of: 5"
#  (23) -> 'Number of: many'
def num_of_items(count):
    total = "many" if count >= 10 else count
    return "Number of: {}".format(total)


# 2.
# Входящие параметры: string s,
# Результат: string из 2х первых и 2х последних символов s
# Пример 'welcome' -> 'weme'.
# 6 -> 66
def start_end_symbols(s):
    if s is None or len(s) == 0:
        return ""
    return s[:2] + s[-2:]


# 3.
# Входящие параметры: string s,
# Результат: string где все вхождения 1го символа заменяются на '*'
# (кроме самого 1го символа)
# Пример: 'bibble' -> 'bi**le'
# s.replace(stra, strb)
def replace_char(s):
    target_char = s[0]
    return s[:1] + s[1:].replace(target_char, '*')


# 4
# Входящие параметры: string a и b,
# Результат: string где <a> и <b> разделены пробелом
# а превые 2 симв обоих строк заменены друг на друга
# Т.е. 'max', pid' -> 'pix mad'
# 'dog', 'dinner' -> 'dig donner'
def str_mix(a, b):
    a_modified = b[:2] + a[2:]
    b_modified = a[:2] + b[2:]
    return "{} {}".format(a_modified, b_modified)


# Provided simple test() function used in main() to print
# what each function returns vs. what it's supposed to return.
def test(res, expt):
    print("Expected {}, was returned {}".format(expt, res))
