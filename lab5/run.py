import sys
import random

from flask import Flask, render_template, request, session, redirect, url_for
from lab5 import image_search

DIFFICULTIES = {"easy": 10, "medium": 50, "difficult": 100}
GUESS_KEY = "guess_key"
NAMES = None

app = Flask(__name__)
app.secret_key = "Haha"


@app.route("/")
def index():
    return render_template("index.html", difficulties=DIFFICULTIES)


@app.route("/start", methods=["GET"])
def start_game():
    difficulty = request.args.get("difficulty")
    if difficulty not in DIFFICULTIES:
        return redirect_to_index()
    else:
        guess_indices = random.sample(range(0, DIFFICULTIES[difficulty] - 1), 5)
        id_to_guess = guess_indices[0]
        session[GUESS_KEY] = id_to_guess

        image_link = image_search.get_random_image(NAMES[id_to_guess])

        random.shuffle(guess_indices)
        return render_template("start.html", names=NAMES, guess_indices=guess_indices, image_link=image_link)


@app.route("/guess", methods=["POST"])
def guess():
    name_index = int(request.form.get("name_index"))

    return render_template("guess.html", guessed=NAMES[name_index], correct=NAMES[session[GUESS_KEY]])


@app.errorhandler(500)
def error_500(e):
    return redirect_to_index()


@app.errorhandler(405)
def error_405(e):
    return redirect_to_index()


def redirect_to_index():
    return redirect(url_for("index"))


def read_names():
    if len(sys.argv) > 1:
        with open(sys.argv[1]) as f:
            lines = f.read().splitlines()
    else:
        raise RuntimeError("Path to names file was not provided")
    return lines


if __name__ == "__main__":
    NAMES = read_names()
    app.run()
