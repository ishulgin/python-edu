import json
import requests
import random

from bs4 import BeautifulSoup

SEARCH_HEADERS = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0'}
IMAGES_LIMIT = 15
CACHE = {}


def fetch_url_and_parse_html(url, headers):
    response = requests.get(url, headers=headers).text
    return BeautifulSoup(response, 'html.parser')


def url_for_query(query):
    return f"https://www.google.ru/search?as_st=y&tbm=isch&safe=active&tbs=isz:vga,islt:qsvga,itp:face&as_q={query}"


def search_images(query):
    if query not in CACHE:
        results = []
        url = url_for_query(query)

        while len(results) == 0:
            parsed_html = fetch_url_and_parse_html(url, SEARCH_HEADERS)

            for index, elem in enumerate(parsed_html.find_all("div", {"class": "rg_meta"})):
                json_result_text = elem.text
                json_result = json.loads(json_result_text)

                image_url = json_result["ou"]

                results.append(image_url)
                if index + 1 == IMAGES_LIMIT:
                    break

        CACHE[query] = results

    return CACHE[query]


def get_random_image(query):
    return random.choice(search_images(query))
