class Feed:
    db_id: int

    def __init__(self, title: str, link: str):
        self.link = link
        self.title = title
