from unittest import TestCase

from lab4 import lab4


class ExtractWordsTest(TestCase):
    def test_single_line(self):
        text = ["asd asd qwe"]
        expected = ["asd", "asd", "qwe"]
        self.assertEqual(expected, lab4.extract_words(text))

    def test_multi_line(self):
        text = ["asd asd qwe\n", "asd asd qwe\n"]
        expected = ["asd", "asd", "qwe", "asd", "asd", "qwe"]
        self.assertEqual(expected, lab4.extract_words(text))


class GenerateTextTest(TestCase):
    def test_generate_text_1(self):
        self.assertEqual("Asd", lab4.generate_text({'': ["asd"]}))
