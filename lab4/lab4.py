import random
import itertools
import sys

PUNCTUATION_MARKS = {'.', '!', '?'}

MAX_GENERATED_WORDS = 40

EMPTY_WORD = ''


def extract_words(text):
    return list(itertools.chain(*map(lambda x: x.split(), text)))


def get_lines():
    if len(sys.argv) == 1:
        lines = sys.stdin.readlines()
    else:
        with open(sys.argv[1]) as f:
            lines = f.readlines()
    return lines


def mem_dict(words):
    res_dict = {}

    prev_word = EMPTY_WORD
    for word in words:
        if prev_word not in res_dict:
            res_dict[prev_word] = []

        if prev_word != EMPTY_WORD:
            res_dict[EMPTY_WORD].append(word)

        res_dict[prev_word].append(word)
        prev_word = word

    return res_dict


def generate_text(follows):
    generated_words = []
    word = random.choice(follows[EMPTY_WORD])
    generated_words.append(word.capitalize())

    while True:
        available_words = follows.get(word, [])
        if len(available_words) > 0:
            word = random.choice(available_words)
            generated_words.append(word)
        else:
            break

        if word in PUNCTUATION_MARKS:
            break

        if len(generated_words) > MAX_GENERATED_WORDS:
            generated_words[-1] += '...'
            break

    generated_words[0] = generated_words[0].capitalize()

    return ' '.join(generated_words)


def main():
    text = get_lines()
    print(f"Source text: {text}")
    words = extract_words(text)

    dictionary = mem_dict(words)
    generated_text = generate_text(dictionary)
    print(f"Generated text: {generated_text}")


if __name__ == '__main__':
    main()
